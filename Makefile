##
# Tad Lispy NixOS configuration
#
# @file
# @version 0.1
include Makefile.d/defaults.mk

help: ## Print this help message
help:
	@
	echo "Useage: make [ goals ]"
	echo
	echo "Available goals:"
	echo
	cat $(MAKEFILE_LIST) | awk -f Makefile.d/make-goals.awk
.PHONY: help

switch: ## Rebuild, then switch
switch: rebuild commit
switch:
	sudo nixos-rebuild switch --flake .#
.PHONY: switch


upgrade: ## Upgrade the system (update, commit, switch)
upgrade: update switch
.PHONY: upgrade


collect-garbage: ## Remove old generations and collect garbage
collect-garbage: older-than ?= 14d
collect-garbage:
	sudo nix profile wipe-history \
	--older-than $(older-than) \
	--profile /nix/var/nix/profiles/system

	nix profile wipe-history \
	--older-than $(older-than) \
	--profile ~/.local/state/nix/profiles/home-manager

	nix store gc

	nix store optimise
.PHONY: collect-garbage

update: is_clean
	nix flake update
.PHONY: update

is_clean:
	@
	if test -n "$$(git status --porcelain)"
	then
		echo "The repository is not clean. First commit or stash your prior changes."
		exit 2
	fi
.PHONY: is_clean


commit: template := $(shell mktemp)
commit: rebuild prompt
	printf "Upgrade\n\n" > $(template)
	nix store diff-closures \
		/nix/var/nix/profiles/system \
		./result \
	| ansifilter \
	>> $(template)
	git add .
	git commit --file=$(template) --edit

	echo "NOTE: The commit has not been pushed. Enter 'git push' to do it."
.PHONY: commit


prompt:
	@
	echo "Here is the summary of channges:"
	echo
	nix store diff-closures  /nix/var/nix/profiles/system ./result
	echo
	read -p "Do you want to continue? [y/N] " answer
	if [ "$${answer}" != "y" ]
	then
		echo "Aborting."
		exit 1
	fi
.PHONY: prompt


rebuild: ## Rebuild the system without switching
rebuild: result
.PHONY: rebuild


result: $(shell find . -iname '*.nix')
result: flake.lock
	nixos-rebuild build --flake .#

# end
