;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!

;; Make sure to use login shell, not the shell that happened to be used when
;; 'doom sync' was called. Context: I am experimenting with Nushell, which is
;; very nice, but not compatible with most scripts.

(defun tad/get-login-shell ()
  "Get the path of the login shell of the current user."
  (string-trim (shell-command-to-string
                "/bin/sh -c 'getent passwd ${USER} | cut -d: -f7'")))

(setq shell-file-name (tad/get-login-shell))


;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name (string-trim (shell-command-to-string "git config user.name"))
      user-mail-address (string-trim (shell-command-to-string "git config user.email")))

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
;;(setq doom-font (font-spec :family "Fira Code" :size 12 :weight 'semi-light)
;;      doom-variable-pitch-font (font-spec :family "Fira Sans" :size 13))
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-one)

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type t)

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")


;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

;; Fix for:
;; - https://github.com/hlissner/doom-emacs/issues/5622
;; - https://github.com/justbur/emacs-which-key/issues/231
(setq which-key-allow-imprecise-window-fit nil)

;; Always keep some lines around the cursor visible.
(setq scroll-margin 5)

(setq delete-by-moving-to-trash t)

;; SEE: https://github.com/alphapapa/magit-todos/issues/185
;; (use-package magit-todos
;;   :after magit
;;   :config (magit-todos-mode 1))

;; Use paredit for lisps

(add-hook! '(lisp-mode-hook
             emacs-lisp-mode-hook
             scheme-mode-hook)
           #'paredit-mode)

;; Highlight code blocks in Markdown
(setq markdown-fontify-code-blocks-natively t)


;; The code block text object
(require 'evil)

(evil-define-text-object evil-a-fenced-block (count beg end type)
  "A fenced block together with fences"
  (markdown-get-enclosing-fenced-block-construct))

(evil-define-text-object evil-inner-fenced-block (count beg end type)
  "Code inside a fenced block"
  (let* ((bounds (markdown-get-enclosing-fenced-block-construct))
         (begin (and bounds (not (null (nth 0 bounds))) (goto-char (nth 0 bounds)) (line-beginning-position 2)))
         (end (and bounds(not (null (nth 1 bounds)))  (goto-char (nth 1 bounds)) (line-beginning-position 1))))
    (list begin end)))

(dolist (state '(visual operator))
  (evil-define-key state evil-markdown-mode-map "ae" 'evil-a-fenced-block)
  (evil-define-key state evil-markdown-mode-map "ie" 'evil-inner-fenced-block))

;; Nicer Markdown fonts

(add-hook 'markdown-mode-hook 'mixed-pitch-mode)

(custom-set-faces!
  '(markdown-header-delimiter-face :foreground "#616161" :height 0.9)
  '(markdown-header-face-1 :height 1.8 :foreground "#A3BE8C" :weight extra-bold :inherit markdown-header-face)
  '(markdown-header-face-2 :height 1.4 :foreground "#EBCB8B" a:weight extra-bold :inherit markdown-header-face)
  '(markdown-header-face-3 :height 1.2 :foreground "#D08770" :weight extra-bold :inherit markdown-header-face)
  '(markdown-header-face-4 :height 1.15 :foreground "#BF616A" :weight bold :inherit markdown-header-face)
  '(markdown-header-face-5 :height 1.1 :foreground "#b48ead" :weight bold :inherit markdown-header-face)
  '(markdown-header-face-6 :height 1.05 :foreground "#5e81ac" :weight semi-bold :inherit markdown-header-face)
  '(markdown-metadata-key-face :foreground "#ff6c6b" :family "monospaced"))

;; Hide markup
(setq-default markdown-hide-urls t)

;;; Spell checking
(add-hook 'spell-fu-mode-hook
          (lambda ()
            (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "nl"))
            (spell-fu-dictionary-add (spell-fu-get-ispell-dictionary "en"))))
