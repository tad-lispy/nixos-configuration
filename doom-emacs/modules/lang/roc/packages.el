;; -*- no-byte-compile: t; -*-
;;; lang/roc/packages.el

(package! roc-mode :recipe (:host gitlab
                            :repo "tad-lispy/roc-mode"
                            :local-repo "~/Projects/roc-mode"))
