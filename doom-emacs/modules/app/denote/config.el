;;; app/denote/config.el -*- lexical-binding: t; -*-

(use-package! denote
  :config
  (map! :leader (:prefix ("d" . "denote")
                 :desc "New note" "n" #'denote
                 :desc "Find backlinks" "b" #'denote-link-find-backlink
                 :desc "Insert a link" "l" #'denote-link-or-create)))

(use-package! denote-menu
  :config
  (map! :leader (:prefix ("d" . "denote")
                 :desc "List notes" "d" #'denote-menu-list-notes)))

(setq! denote-file-type 'markdown-yaml)

(setq! denote-excluded-directories-regexp "dist")
