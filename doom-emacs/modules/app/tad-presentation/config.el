;;; app/tad-presentation/config.el -*- lexical-binding: t; -*-

;;; Tad Presentation
;; Use ]] and [[ for a simple org-mode and markdown presentations
;;
;; TODO: Move to own module

;; Tad Presentation for org-mode
;;
(defun tad/org-reveal ()
  "Show current entry, and it's context, keeping other entries folded."
  (interactive)
  (org-overview)
  (org-fold-show-entry)
  (org-fold-show-children)
  (tad/org-reveal-context))

(defun tad/org-reveal-context ()
  "Walk up the tree, showing all children on each level"
  (interactive)
  (save-excursion (while (and (org-get-heading) (> (org-current-level) 1))
                    (org-up-heading-all 1)
                    (org-fold-show-children))))

(defun tad/org-present-forward ()
  "Move to the next heading, reveal it and fold everything else"
  (interactive)
  (org-next-visible-heading 1)
  (tad/org-reveal))

(defun tad/org-present-backward ()
  "Move to the next heading, reveal it and fold everything else"
  (interactive)
  (org-previous-visible-heading 1)
  (tad/org-reveal))

(map! :after org
      :map org-mode-map
      :m
      "] ]" #'tad/org-present-forward)

(map! :after org
      :map org-mode-map
      :m
      "[ [" #'tad/org-present-backward)


;; Tad Presentation for markdown

(defun tad/markdown-present-forward ()
  "Move to the next heading, reveal it and fold everything else"
  (interactive)
  (outline-show-all)
  (outline-next-heading)
  (outline-hide-other)
  (outline-show-children))

(defun tad/markdown-present-backward ()
  "Move to the next heading, reveal it and fold everything else"
  (interactive)
  (outline-show-all)
  (outline-previous-heading)
  (outline-hide-other)
  (outline-show-children))

(map! :after outline
      :map markdown-mode-map
      :m
      "] ]" #'tad/markdown-present-forward)

(map! :after outline
      :map markdown-mode-map
      :m
      "[ [" #'tad/markdown-present-backward)

(map! :after outline
      :map markdown-mode-map
      :m
      "M-<up>" #'outline-move-subtree-up)

(map! :after outline
      :map markdown-mode-map
      :m
      "M-<down>" #'outline-move-subtree-down)
