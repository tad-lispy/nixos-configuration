;;; app/ollama/config.el -*- lexical-binding: t; -*-

(use-package! ellama
  :config
  (map! :leader (:prefix ("A" . "AI")
                 :desc "Ask about" "A" #'ellama-ask-about
                 :desc "Ask" "I" #'ellama-chat
                 :desc "Complete" "C" #'ellama-complete
                 :desc "Define word" "D" #'ellama-define-word
                 :desc "Summarize" "S" #'ellama-summarize
                 :desc "Summarize a web page" "W" #'ellama-summarize-webpage
                 :desc "Select a provider" "P" #'ellama-provider-select))
  (setopt ellama-provider
          (make-llm-ollama
           ;; this model should be pulled to use it
           ;; value should be the same as you print in terminal during pull
           :chat-model "deepseek-r1:14b"))
  (setq! ellama-major-mode 'markdown-mode))

;; Extra key bindings for the chat (session mode)
(setq! ellama-session-mode-map
       (make-sparse-keymap "ellama-session-mode-map"))

(map! :map ellama-session-mode-map
      :inv "C-<return>" #'ellama-chat-send-last-message)

(setf (alist-get 'ellama-session-mode minor-mode-map-alist)
      ellama-session-mode-map)
