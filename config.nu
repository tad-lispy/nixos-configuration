# Create a remote repository on the home server
def "homeserver git remote add" [
    --host-name = "home.local"  # The home server host name
    --user-name: string         # The username on the home server (for SSH; defaults to the local user name)
    --branch = "main"           # The main branch
] {
  let project_path = pwd | path relative-to ~/
  let user_name = ($user_name | default (whoami))


  ssh $"($user_name)@($host_name)" git init --bare $"($project_path)" --initial-branch $"($branch)"
  git remote add origin $"($user_name)@($host_name):($project_path)"
}
