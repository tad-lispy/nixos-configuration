{ config, pkgs, lib, ... }:
let
  my-aspell = (pkgs.aspellWithDicts (dicts: [
    dicts.en
    dicts.en-computers
    dicts.pl
    dicts.nl
  ]));
in {
  home.language.base =  "en_US.UTF-8";

  programs.alacritty.enable = true;

  programs.nnn.enable = true;

  programs.firefox.enable = true;
  programs.firefox.package = pkgs.firefox-wayland;

  systemd.user.sessionVariables = {
    EDITOR="nvim";

    # firefox screencapture
    MOZ_ENABLE_WAYLAND="1";
    MOZ_USE_XINPUT2="1";

    # SDL
    SDL_VIDEODRIVER="wayland";

    # QT
    QT_QPA_PLATFORM="wayland";
  };

  home.shellAliases = {
    open = "xdg-open";
  };

  programs.neovim = {
    enable = true;
    coc.enable = true;
    defaultEditor = true;
    vimAlias = true;
    vimdiffAlias = true;
  };

  programs.nushell.enable = true;
  # Fix for https://github.com/rsteube/carapace-bin/issues/1897
  programs.nushell.extraEnv = ''
    $env.ENV_CONVERSIONS = {
        "PATH": {
            from_string: { |s| $s | split row (char esep) | path expand --no-symlink }
            to_string: { |v| $v | path expand --no-symlink | str join (char esep) }
        }
        "Path": {
            from_string: { |s| $s | split row (char esep) | path expand --no-symlink }
            to_string: { |v| $v | path expand --no-symlink | str join (char esep) }
        }
    }
  '';
  programs.nushell.configFile.source = ./config.nu;
  programs.carapace = {
    enable = true;
    enableNushellIntegration = true;
  };
  programs.starship = {
    enable = true;
    enableNushellIntegration = true;
  };

  programs.atuin.enable = true;

  programs.zsh = {
    enable = true;
    defaultKeymap = "viins"; # Doesn't take effect so I set it again in initExtra
    syntaxHighlighting.enable = true;
    initExtra = ''
      # Vi mode
      # https://dougblack.io/words/zsh-vi-mode.html

      bindkey -v

      # Press v in normal mode to edit command line in Vim
      # autoload -U edit-command-line
      # zle -N edit-command-line
      # bindkey -M vicmd v edit-command-line

      bindkey "^[[A" history-beginning-search-backward # Up
      bindkey "^[[B" history-beginning-search-forward  # Down

      bindkey '^P' up-history
      bindkey '^N' down-history
      bindkey '^?' backward-delete-char
      bindkey '^h' backward-delete-char
      bindkey '^w' backward-kill-word
      bindkey '^r' history-incremental-search-backward
      bindkey '^g' per-directory-history-toggle-history
      bindkey '^a' insert-last-word

      # https://archive.emily.st/2013/05/03/zsh-vi-cursor/
      # https://stackoverflow.com/questions/44534678/how-to-change-cursor-shape-depending-on-vi-mode-in-bash
      function zle-keymap-select zle-line-init
      {
          # change cursor shape in iTerm2
          case $KEYMAP in
              vicmd)      print -n -- "\1\e[2 q\2";;  # block cursor
              viins|main) print -n -- "\1\e[6 q\2";;  # line cursor
          esac

          zle reset-prompt
          zle -R
      }

      function zle-line-finish
      {
          print -n -- "\e[2 q" # block cursor
      }

      zle -N zle-line-init
      zle -N zle-line-finish
      zle -N zle-keymap-select
      export KEYTIMEOUT=1

      # Update clock in the prompt just before executing the
      # command
      update-propmt-and-accept-line() {
        zle autosuggest-clear
        zle reset-prompt
        zle accept-line
      }
      zle -N update-propmt-and-accept-line
      bindkey "^M" update-propmt-and-accept-line
    '';
    oh-my-zsh =  {
      enable = true;
      theme = "amuse";
      plugins = [
        "per-directory-history"
      ];
    };
  };

  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
    enableNushellIntegration = true;
  };

  programs.tealdeer.enable = true;

  programs.obs-studio = {
    enable = true;
  };
  programs.bat.enable = true;
  programs.bat.extraPackages = [ pkgs.bat-extras.batman ];
  programs.lsd.enable = true;
  programs.lsd.enableAliases = true;
  programs.tmux = {
    enable = true;
    shell = "${pkgs.nushell}/bin/nu";
    prefix = "C-Space";
    keyMode = "vi";
    historyLimit = 10 * 1000;
    clock24 = true;
    extraConfig = ''
      setw -g mouse on
      bind -T copy-mode-vi y send-keys -X copy-pipe-and-cancel 'xclip -in -selection clipboard'
      bind -T copy-mode-vi 'v' send -X begin-selection

      bind -r j select-pane -D
      bind -r k select-pane -U
      bind -r h select-pane -L
      bind -r l select-pane -R

      bind | split-window -h -c "#{pane_current_path}"
      bind - split-window -v -c "#{pane_current_path}"

      bind -r J resize-pane -D 5
      bind -r K resize-pane -U 5
      bind -r H resize-pane -L 5
      bind -r L resize-pane -R 5
    '';
  };
  programs.helix.enable = true;

  programs.gpg = {
    enable = true;
    # TODO: Consider mutableKeys = false;
  };
  services.gpg-agent = {
    enable = true;
    pinentryPackage = pkgs.pinentry-gnome3;
  };

  systemd.user.services.ollama = {
    Unit = {
      Description = "Ollama AI assistant service";
    };

    Service = {
      ExecStart = "${pkgs.ollama-rocm}/bin/ollama serve";
    };

    Install = {
      WantedBy = [ "default.target" ];
    };
  };

  home.packages = [
    pkgs.shortwave
    pkgs.thunderbird
    pkgs.nvtop-amd
    pkgs.mousam
    pkgs.pika-backup
    pkgs.borgbackup
    pkgs.devenv
    pkgs.git
    pkgs.git-lfs
    pkgs.killall
    pkgs.cloc
    pkgs.nixd
    pkgs.sqlite
    pkgs.podman
    pkgs.podman-compose
    pkgs.dialect
    pkgs.gnome-pomodoro
    pkgs.gnome-boxes
    pkgs.wl-clipboard
    pkgs.vlc
    pkgs.ripgrep
    pkgs.fd
    pkgs.gnome-sound-recorder
    pkgs.languagetool
    pkgs.jdk11
    pkgs.cachix
    pkgs.gimp
    pkgs.krita
    pkgs.inkscape
    pkgs.httpie
    pkgs.chromium
    pkgs.blender
    pkgs.rocmPackages.rocminfo
    pkgs.signal-desktop
    # pkgs.delta
    pkgs.rocmPackages.clr
    pkgs.vulkan-loader
    pkgs.gnomeExtensions.gsconnect
    pkgs.gnomeExtensions.pano
    pkgs.eyedropper
    pkgs.trashy
    pkgs.texlive.combined.scheme-medium
    pkgs.airshipper
    pkgs.rnote
    pkgs.ollama
    pkgs.gcc
    pkgs.fractal
    pkgs.zed-editor
    pkgs.difftastic

    my-aspell

    # Fonts
    # pkgs.nerdfonts
    # pkgs.nerd-fonts.agave
    # pkgs.nerd-fonts.fira-code
    # pkgs.nerd-fonts.fira-mono
    # pkgs.nerd-fonts.inconsolata
    # pkgs.nerd-fonts.recursive-mono
    # pkgs.nerd-fonts.shure-tech-mono
  ] ++ builtins.filter lib.attrsets.isDerivation (builtins.attrValues pkgs.nerd-fonts);

  # Emacs
  programs.emacs = {
    enable = true;
    package = pkgs.emacs30-pgtk;
    extraPackages = emacs-packages: [
      emacs-packages.vterm
    ];
  };

  home.file.".aspell.conf".text = ''
    dict-dir ${my-aspell}/lib/aspell
  '';

  # https://difftastic.wilfred.me.uk/
  programs.git.difftastic = {
    enable = true;
    background = "dark";
  };

  # Pandoc
  programs.pandoc.enable = true;


  # This repository also houses my Doom Emacs configuration, so I could go  full
  # Nix and have the following:
  #
  #     home.file.doom-emacs-config = {
  #       recursive = false;
  #       source = ./doom-emacs;
  #       target = ".config/doom-test";
  #     };
  #
  #
  # The problem with this approach is that the link would point to a Nix store
  # path and target files would be read-only (so not editable with `SPC f p`).
  # Instead any changes would need to be done in the repository and applied with
  # `nixos-rebuild`. I don't think it's very ergonomic, so for now I just have a
  # symbolic link that I made manually with:
  #
  #    $ ln --symbolic ~/nixos/doom-emacs/ ~/.config/doom
  #
  #
  # This is nice, because after editing the doom files I can use magit to commit
  # to the NixOS configuration repository straight from Emacs. And Doom Emacs has
  # declarative configuration anyway, so I don't see much value in involving Nix
  # on top of it.

  # Hide cursor when typing
  services.unclutter.enable = true;

  # Syncthing
  services.syncthing.enable = true;

  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "tad";
  home.homeDirectory = "/home/tad";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.03";
}
