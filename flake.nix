{
  description = "The amazing NixOS configuration of Tad Lispy";

  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    home-manager.url = "github:nix-community/home-manager/master";
    home-manager.inputs.nixpkgs.follows = "nixpkgs";
  };

  outputs = { self, nixpkgs, home-manager, ... }@inputs:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs {
        inherit system;
        config = {
          rocmSupport = true;
        };
      };
    in {
      devShells.${system}.default = pkgs.mkShell {
        name = "nixos-configuration-shell";
        packages = with pkgs; [
          gnumake
          ansifilter # TODO: Remove ansifilter once https://github.com/NixOS/nix/issues/4626 is addressed
        ];
      };
      nixosConfigurations = {
        tad-lispy-nixos = nixpkgs.lib.nixosSystem {
          inherit system;
          specialArgs = { inherit inputs; };
          modules = [
            ./configuration.nix
            home-manager.nixosModules.home-manager
            {
              home-manager.useGlobalPkgs = true;
              home-manager.useUserPackages = true;
              home-manager.users.tad = import ./tad.nix;
            }
          ];
        };
      };
    };
}
